# Phần mission

## Thông tin

git repo: <git@gitlab.com:mkac-agv/mission_manager.git>

## Trạng thái hiện tại

### Input

- Định dạng `json`, chi tiết như sau:

#### Cấu trúc chung của một mission:

```
{
  "name": "<Tên của mission>",
  "group": "move",
  "actions":
  [
    {
      <action 1>
    },
    ...
  ]
}
```

Trong đó:
-  `name`: (_`unique`_) dùng để phân biệt các mission
-  `group` _chưa có ảnh hướng tới chương trình (theo thiết kế của MIR)_
-  `actions` chứa mảng thứ tự các action sẽ thực hiện trong mission này.

#### Action

- Cấu trúc chung:
```
    {
      "name": "move to PickC",
      "type": "move",
      "params": {
          "start": "Move",
          "stop": "PickC",
          "retries": 5,
          "distance_threshold": 0.05
      }
    }
```
Trong đó:
- `name`: Tên của action
- `type`: Kiểu action, sử dụng để phân loại xử lý trong mission_server.
  - `move`: Kiểu di chuyển
  - `pickup`: Lấy xe kiểu nâng từ dock
  - `droping`: Hạ thả xe vào dock (_Nên đặt là `Dropdown`_)
  - `charging`: Vào dock sạc và quản lý hành động sạc
  - `safety_setting`: Thay đổi `footprint` và `current_job` của `scan_safety`

1. `move`:
```
    {
      "name": "move to PickC",
      "type": "move",
      "params": {
          "start": "Move",
          "stop": "PickC",
          "retries": 5,
          "distance_threshold": 0.05
      }
    }
```

- `start` và `stop` tương ứng với cột Start và Stop trong `path_creator`
- `distance_threshold`: sai số vị trí cho phép theo khoảng cách Euclid
- `retries`: Số lần thử để đạt được sai số trên.
* _2 tham số cuối chưa code_

Phương pháp:
- dump thành phần `params` ra và publish vào topic `run_single_path`
- Result xét phản hồi tại: `follow_waypoints/result`

2. `pickup`
```
    {
      "name": "Pick shelf 1 at C",
      "type": "pickup",
      "params": {
        "marker_pos": "PickC",
        "marker_type": "VL",
        "shelf_footprint": "footprint1",
        "safety_job": 3,
        "undocking_distance": 1.0
      }
    }
```
- Action này vẫn sử dụng cấu hình được cài đặt trong path_creater. `type` tương ứng với cột `Start`, `marker_pos` tương ứng với cột `Stop`.
- `marker_type`: Kiểu marker (VL, Bar, Leg...)
- `self_footprint` và `safety_job`: lần lượt là tên của footprint và job cần thay đổi trong scan_safety. (_Phần này chưa dùng_)
- `undocking_distance`: Khoảng cách mà rb sẽ lùi ra sau khi nâng bàn nâng để lấy xe. (_chưa dùng_)

- Chu trình lấy xe:
  - Di chuyển tới điểm phía trước dock xe
  - Thay đổi safety job về job có vùng quét nhỏ để rb có thể di chuyển vào dock.
  - Detect vị trí của marker
  - Di chuyển tương đối tới vị trí có thể lấy xe (di chuyển theo odom)
  - Nâng bàn nâng
  - undock: Tịnh tiến lùi ra khỏi dock
  - Thiết lập footprint theo footprint của xe (để `scan_safety` bỏ qua chân xe)

- Chu trình lấy xe hiện tại: các phần trên rời rạc, chưa tích hợp thành 1 function.

3. `droping`
```
    {
      "name": "dropdown shelf 1 at C",
      "type": "droping",
      "params": {
        "marker_pos": "DropC",
        "marker_type": "VL",
        "safety_job": 3,
        "undocking_distance": 1.0
      }
    }
```
Tương tự `pickup`.
Sau khi thả xe xong thì đặt về footprint gốc của rb

4. `charging`
```
    {
      "name": "TramSac2",
      "type": "charging",
      "params":{
        "charger_name": "<Tên trạm sạc>"
        "minimum_time": 60,
        "minimum_percentage": 80,
        "charge_until_new_mission": false
      }
    }
```
Action thực hiện đưa xe vào dock sạc tự động

- Action này sử dụng cặp thông tin trong tag `type` và `charger_name` tương ứng với cột Start và Stop trong path_creater
- `minimum_time`: Thời gian sạc tối thiểu (s) cho tới khi cho phép di chuyển ra khỏi sạc. (_Chưa có_)
- `minimum_percentage`: Phần trăm sạc tối thiểu (%) cho phép di chuyển rb ra khỏi sạc để thực hiện mission khác.
- `charge_until_new_mission`: (bool) cho phép hay không cho phép robot di chuyển ra khỏi sạc ngay khi có mission mới.

- Quy trình thực hiện hành động sạc:
  - Di chuyển tới điểm phía trước dock sạc (move)
  - Detect marker để xác định vị trí dock sạc
  - Di chuyển tương đối tới vị trí dock sạc để chấu sạc tiếp xúc với phần sạc trên robot.
  - Nếu `charge_until_new_mission` = True thì robot có thể `undock` ngay khi có mission mới để di chuyển thực hiện mission mới.
  - Nếu không, phải đạt được một trong 2 điều kiện `minimum_time` hoặc `minimum_percentage` rồi mới được `undock`.

(Quy trình hiện tại mới thực hiện tới bước cho robot di chuyển lùi vào dock sạc)


5. `safety_setting`
```
    {
      "name": "Safety setting",
      "type": "safety_setting",
      "params":{
        "footprint": "shelf_1",
        "safety_job": 3
      }
    }
```
Dùng để thay đổi `footprint` và `current_job` của `scan_safety`.
- `params/footprint`: footprint dưới dạng string (vd: `"[[0.6, 0.5], [-0.6, 0.5], [-0.6, -0.5], [0.6, -0.5], [0.6, 0.5]]"`)
- `params/safety_job`: (int) chọn job.


## Kiến trúc
![](imgs/AMR_Mission_structure.jpg)